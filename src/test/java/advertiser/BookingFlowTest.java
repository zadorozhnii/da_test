package advertiser;

import model.UserData;
import org.testng.annotations.Test;
import utils.TestConfig;
import steps.Advertiser;

public class BookingFlowTest extends TestConfig {
    @Test (dataProvider = "credentials", dataProviderClass = UserData.class)
    public void productBookingFlow(String login, String password) {

        Advertiser advertiser = new Advertiser(driver, login, password);
        advertiser.login(advertiser);
        advertiser.bookProduct();


        /*
        * It needs to be refactored
        Publisher publisher = new Publisher();
        publisher.login(publisher);
        Advertiser advertiser = publisher.createAdvertiser();
        publisher.logout();
        advertiser.activate();
        advertiser.checkShoppingCartData();
        advertiser.checkPurchaseFunctionality();
        advertiser.checkBookingListing();
        advertiser.checkBookingDetailedPage();
        advertiser.checkProductListing();
        advertiser.checkProductDetailedPage();
        */
    }
}