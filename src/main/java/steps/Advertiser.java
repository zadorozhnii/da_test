package steps;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.DashboardPage;
import pages.ProductBookingPage;

public class Advertiser extends AcceptanceTester {

    public Advertiser(EventFiringWebDriver driver, String login, String password) {
        super(login, password, driver);
    }

    /**
     *
     * Method books product in the Booking page
     */
    public void bookProduct() {
        new DashboardPage(driver).openBookingPage();
        ProductBookingPage productBookingPage = new ProductBookingPage(driver);
        productBookingPage
                .inputOrderName()
                .selectProduct()
                .enterGeoTargeting()
                .addDayPart()
                .addFrequencyCapping()
                .addStartDate()
                .addEndDate()
                .setBudget()
                .setTimeZone()
                .addCreativeLater()
                .checkAvailability()
                .checkSideBar()
                .pressCreateButton();
    }


    /**
     *
     * Method checks data in the shopping cart page after purchase
     */
   public void checkShoppingCartData() {
       //TODO
   }

}