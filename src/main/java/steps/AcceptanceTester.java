package steps;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.DashboardPage;
import pages.SignUpPage;

public class AcceptanceTester {
    public static String email;
    public static String password;
    private By adminIcon = By.cssSelector(".fa.fa-user");
    private By logoutButton = By.xpath("//a[contains(.,'Sign out')]");
    EventFiringWebDriver driver;

    AcceptanceTester(String email, String password, EventFiringWebDriver driver){
        this.email = email;
        this.password = password;
        this.driver = driver;
    }

    public DashboardPage login(AcceptanceTester tester) {
       new SignUpPage(driver)
               .enterEmail(tester)
               .enterPassword(tester)
               .clickLogin();
        return new DashboardPage(driver);
    }

    public void logout() {
        driver.findElement(adminIcon).click();
        driver.findElement(logoutButton).click();
    }
}