package model;

import org.testng.annotations.DataProvider;

public class UserData {

        private static final String ADVER_LOGIN = "advertiserdanads+dir@gmail.com";
        private static final String ADVER_PASSWORD = "Aa1111";


        @DataProvider
        private static Object[][] credentials(){
            return new Object[][]{{getLogin(), getPassword()}};
        }

        /**
         *
         * @return The password of the advertiser console.
         */
        public static String getPassword() {
            return ADVER_PASSWORD;
        }

        /**
         *
         * @return The login of the advertiser console.
         */
        public static String getLogin() {
            return ADVER_LOGIN;
        }
}
