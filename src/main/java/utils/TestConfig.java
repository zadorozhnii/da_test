package utils;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Reporter;
import org.testng.annotations.*;
import utils.logging.EventHandler;
import java.util.concurrent.TimeUnit;

public class TestConfig {
    protected EventFiringWebDriver driver;


    @BeforeClass
    @Parameters("browser")
    public void setUp(String browser) {
        driver = new EventFiringWebDriver(DriverFactory.initDriver(browser));
        driver.register(new EventHandler());
        Reporter.setEscapeHtml(false);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Properties.getBaseUrl());
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }
}