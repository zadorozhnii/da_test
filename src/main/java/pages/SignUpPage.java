package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import steps.AcceptanceTester;

public class SignUpPage {
    private By emailField = By.name("email");
    private By passwordField = By.name("password");
    private By loginButton = By.xpath("//input[@value = 'Login']");
    private EventFiringWebDriver driver;

    public SignUpPage(EventFiringWebDriver driver){
        this.driver = driver;
    }

    public SignUpPage enterEmail(AcceptanceTester tester) {
       driver.findElement(emailField).sendKeys(tester.email);
       return new SignUpPage(driver);
    }

    public SignUpPage enterPassword(AcceptanceTester tester) {
        driver.findElement(passwordField).sendKeys(tester.password);
        return new SignUpPage(driver);
    }

    public DashboardPage clickLogin(){
        driver.findElement(loginButton).click();
        return new DashboardPage(driver);
    }
}
