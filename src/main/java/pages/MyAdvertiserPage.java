package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class MyAdvertiserPage {

    public static String URL = "http://vm-da-04.ipa.dev.sigmaukraine.com:8088/advertisers";
    private String createNewButton = "//a[contains(.,'Create new')]";
    private EventFiringWebDriver driver;

    public MyAdvertiserPage(EventFiringWebDriver driver){
        this.driver = driver;
    }

    public MyAdvertiserPage openPage (){
        driver.get(MyAdvertiserPage.URL);
        return new MyAdvertiserPage(driver);
    }

    public MyAdvertiserPage pressCreateNewButton(){
        driver.findElement(By.id(createNewButton)).click();
        return new MyAdvertiserPage(driver);
    }
}
