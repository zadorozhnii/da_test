package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class DashboardPage {

  private By bookProductButton = By.xpath("//a[contains(.,'Book a product')]");
  EventFiringWebDriver driver;

  public DashboardPage(EventFiringWebDriver driver){
      this.driver = driver;
    }

    public ProductBookingPage openBookingPage(){
        driver.findElement(bookProductButton).click();
        return new ProductBookingPage(driver);
    }

}
