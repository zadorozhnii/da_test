package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.util.Date;

public class ProductBookingPage {
    private EventFiringWebDriver driver;
    private Integer productBudget = 555;
    private Integer cpmProductValue = 8;
    private String productName = "BLM_Black_2509_SEK";
    private String generalSettingsTitle = "//h1[contains(.,'General Settings')]";
    private String targetingoptionsTitle = "//h1[contains(.,'Targeting options')]";
    private String scheduleAndBudgetTitle = "//h1[contains(.,'Schedule and Budget')]";
    private String uploadCreativeTitle = "//h1[contains(.,' Upload creative')]";
    private String orderNameInputField = "general.general--campaignName";
    private String selectProduct = "//select[@id='general.general--product_id']";
    private String selectLocation = "//*[@id='targetingOptions']//li/input";
    private String addDaypartButton = "//a[contains(.,'Add a new Daypart')]";
    private String addDaypartDayOfWeekButton = "//label[contains(.,\"Day of Week\")]/following-sibling:: select";
    private String frequencyCappingCheckbox = "//label[contains(.,'Set per user frequency cap')]";
    private String startDate = "";
    private String endDate = "";
    private String timeZone = "//select[@id='payment.payment--schedule--timezoneId']";
    private String budgetImpressionInputField = "//*[@id='payment.payment--cost--budget']";
    private String checkAvailabilityButton = "//button[contains(.,\" Check availability\")]";
    private String addCreativeLaterCheckbox = "//label[contains(.,\"Add creative later\")]";
    private String createProductButton = "//button[contains(.,'Create')]";
    //Side bar
    private String timeTargetingValue = "//span[@id=\"placeholder-preview-available\"]";
    private String frequencyCappingValue = "//span[@class='frequencyCapping value']";
    private String productValue = "//span[contains(.,\"Product\")]/following-sibling::span";
    private String budgetValue = "//span[contains(.,'Budget')]/following-sibling::span";
    private String impessionsValue = "//span[contains(.,'Target impressions')]/following-sibling::span";
    private String cpmValue = "//span[contains(.,'CPM')]/following-sibling::span";
    //cart
    private String cartSuccessMessage = "//p[contains(.,'Product has been created')]";
    private String someText = "asdf";

    public ProductBookingPage(EventFiringWebDriver driver){
        this.driver = driver;
    }

    /**
     *
     * Method inputs order of a campaign.
     */
    public ProductBookingPage inputOrderName() {
        driver.findElement(By.id(orderNameInputField)).sendKeys("product for advertiser" + " " + new Date().getTime());
        return new ProductBookingPage(driver);
    }

    /**
     *
     * Method selects a product during purchase of a campaign.
     */
    public ProductBookingPage selectProduct() {
        driver.findElement(By.id(selectProduct));
        return new ProductBookingPage(driver);
    }

    /**
     *
     * Method entered geotargeting value during campaign purchasing.
     */
    public ProductBookingPage enterGeoTargeting() {
        driver.findElement(By.id(selectLocation)).click();
        driver.findElement(By.id(selectLocation)).sendKeys(someText);
        return new ProductBookingPage(driver);
    }

    /**
     *
     * Method sets day part value during campaign purchasing.
     */
    public ProductBookingPage addDayPart() {
        driver.findElement(By.id(addDaypartButton)).click();
        driver.findElement(By.id(addDaypartDayOfWeekButton)).click();
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage addFrequencyCapping() {
       driver.findElement(By.id(frequencyCappingCheckbox)).click();
       return new ProductBookingPage(driver);
    }

    public ProductBookingPage addStartDate() {
       return new ProductBookingPage(driver);
    }

    public ProductBookingPage addEndDate() {
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage setTimeZone() {
        driver.findElement(By.id(timeZone)).click();
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage setBudget() {
        driver.findElement(By.id(budgetImpressionInputField)).click();
        driver.findElement(By.id(budgetImpressionInputField)).clear();
        driver.findElement(By.id(budgetImpressionInputField)).sendKeys(productBudget.toString());
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage addCreativeLater() {
        driver.findElement(By.id(addCreativeLaterCheckbox)).click();
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage checkAvailability() {
        driver.findElement(By.id(checkAvailabilityButton)).click();
        return new ProductBookingPage(driver);
    }

    public CartPage pressCreateButton() {
        waitForElementClickable(By.xpath(createProductButton));
        driver.findElement(By.id(createProductButton)).click();
        waitForElementVisible(By.xpath(cartSuccessMessage));
        return new CartPage();
    }

    public ProductBookingPage openPage() {

        return new ProductBookingPage(driver);
    }

    public ProductBookingPage checkTimeTargetingSideBarValue() {
        Assert.assertEquals( driver.findElement(By.id(timeTargetingValue)).getText(),"Wednesday, 12:00 AM - 11:59 PM");
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage checkFrequencycappingSideBarValue() {
        Assert.assertEquals( driver.findElement(By.id(frequencyCappingValue)).getText(),"1 Impression per 1 Day");
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage checkProductSideBarValue() {
        Assert.assertEquals( driver.findElement(By.id(productValue)).getText(),productName);
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage checkBudgetValue() {
        Assert.assertEquals( driver.findElement(By.id(budgetValue)).getText(),productBudget);
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage checkTimePeriodValue() {
        return new ProductBookingPage(driver);
}

    public ProductBookingPage checkImperssionsValue() {
        Double impressionsQuantity = (productBudget.doubleValue() / cpmProductValue.doubleValue() * 1000);
        Integer impressionQuentityInt = impressionsQuantity.intValue();
        String string =  driver.findElement(By.id(impessionsValue)).getText().replaceFirst(",","");
        assert (impressionQuentityInt.toString().equals(string));
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage checkCPMValue() {
        Assert.assertEquals( driver.findElement(By.id(cpmValue)).getText(),cpmProductValue.toString());
        return new ProductBookingPage(driver);
    }

    public ProductBookingPage checkSideBar() {
        checkTimeTargetingSideBarValue();
        checkFrequencycappingSideBarValue();
        checkProductSideBarValue();
        checkBudgetValue();
        checkTimePeriodValue();
        checkImperssionsValue();
        checkCPMValue();
        return new ProductBookingPage(driver);
    }

    /**
     *
     * Method waits for element becomes clickable.
     */
    public void waitForElementClickable(By by) {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(by));
    }

    /**
     *
     * Method waits for element becomes visible
     */
    public void waitForElementVisible(By by) {
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }
}

